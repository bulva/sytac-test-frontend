import { Component, OnInit, OnDestroy } from '@angular/core';
import 'rxjs/add/operator/takeUntil';
import { Subject } from 'rxjs/Subject';

import { ApiService } from './api.service';

interface ModOfTransportation {
  id: number;
  type: string;
  brand: string;
  colors: Array<string>;
  img: string;
}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.sass']
})
export class AppComponent implements OnInit, OnDestroy {
  private ngUnsubscribe: Subject<void> = new Subject<void>();

  title = 'Trafficmeister';

  // definitions of the selector variables
  modeTypes: Array<string>;
  modeBrands: Array<string>;
  modeColors: Array<string>;

  // definitions of all selector models 
  modeTypeSelected: string;
  modeBrandSelected: string;
  modeColorSelected: string;

  // image url handle
  imageUrl: string;

  // definitions of cache and immutableSelectorType
  cache: Array<ModOfTransportation>;
  immutableSelectorTypes: Array<string>;

  // error handle
  apiError: boolean = false;

  constructor(
    private apiService: ApiService
  ) { }

  ngOnInit() {
    // load and filter data when page loads
    this.populateForm();
  }

  populateForm = (reset?: boolean): void => {

    // resets the form if you toggle between vehicle type
    if (reset) {
      this.modeBrandSelected = null;
      this.modeColorSelected = null;
    }

    // each repopulation will clear 
    // the selectors arrays
    if (this.modeTypes) {
      this.modeTypes.length = 0;
    }
    if (this.modeBrands) {
      this.modeBrands.length = 0;
    }
    if (this.modeColors) {
      this.modeColors.length = 0;
    }

    // clear image 
    this.imageUrl = '';

    // if we don't have data loaded load it
    if (!this.cache) {

      // use service to pull data from API
      this.apiService.getData()
      .takeUntil(this.ngUnsubscribe)
      .subscribe(
        (_mods: Array<ModOfTransportation>) => {
          // save data for reuse within this component
          this.cache = _mods;

          // separate vehicle types for the immuteable selector
          this.immutableSelectorTypes = _mods.map((_mod: ModOfTransportation) => {
            return _mod.type;
          }).filter(this.MakeAllElementsUnique);

          // and process
          this.processData(this.cache);
        },
        (error) => {
          this.apiError = true;
        }
      );
    } 
    else {
      // we already have data so just process data
      this.processData(this.cache);
    }
  }

  // main algorithm with a complexity of linear O(n)
  processData = (_mods: Array<ModOfTransportation>): void => {

      // main cascade filter 
      // 3 stages based on 3 selectors
      let filteredMods = _mods.filter((_mod: ModOfTransportation) => {
        // 1 stage vehicle type
        if (!!this.modeTypeSelected && this.modeTypeSelected === _mod.type){
          return true;
        } 
        else if (!this.modeTypeSelected) {
          return true;
        } 
        else {
          return false;
        }
      }).filter((_mod: ModOfTransportation) => {
        // 2 stage brand
        if (!!this.modeBrandSelected && this.modeBrandSelected === _mod.brand) {
          return true;
        }
        else if (!this.modeBrandSelected) {
          return true;
        }
        else {
          return false;
        }
      }).filter((_mod: ModOfTransportation) => {
        // 3 stage colors
        if (!!this.modeColorSelected && _mod.colors.indexOf(this.modeColorSelected) !== -1) {
          return true;
        }
        else if (!this.modeColorSelected) {
          return true;
        }
        else {
          return false;
        }
      });

      // at this point we have an completely 
      // filtered array containing only 
      // data based on our selection

      // next we are separating the results to 3 selector arrays
      // and making the array values unique
      this.modeTypes = filteredMods.map((_mod: ModOfTransportation) => {
        return _mod.type;
      }).filter(this.MakeAllElementsUnique);

      this.modeBrands = filteredMods.map((_mod: ModOfTransportation) => {
        return _mod.brand;
      }).filter(this.MakeAllElementsUnique);

      let dirtyColors: string[] = [];
      const blackWhole: any = filteredMods.map((_mod: ModOfTransportation) => {
        // with colors we need to concatenate the arrays as colors are already array
        dirtyColors = dirtyColors.concat(_mod.colors);
      });

      // and then clean resulting array with unique filter
      this.modeColors = dirtyColors.filter(this.MakeAllElementsUnique);

      // reinforce selection of the vehicle type
      // the user experience suffers otherwise
      // the vehicle type would have always be "car" 
      // no matter what what you picked in ther fields
      if (this.modeTypes.length === 1) {
        this.modeTypeSelected = this.modeTypes[0];
      }

      // in the case that we only have one result show image
      if (filteredMods.length === 1) {
        this.imageUrl = filteredMods[0].img;
      }
  }

  // simple unique filter function
  MakeAllElementsUnique = (arrayElement: string, index: number, array: Array<string>): boolean => {
    return array.indexOf(arrayElement) === index;
  }

  // destroy cache and kill all subscribed
  ngOnDestroy() {
    this.cache = null;
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }
}
