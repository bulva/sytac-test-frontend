import { TestBed, async, ComponentFixture, inject } from '@angular/core/testing';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';

import { AppComponent } from './app.component';
import { ApiService } from './api.service';

const data = [
  {
    id: 1,
    type: 'car',
    brand: 'Bugatti Veyron',
    colors: ['red', 'black'],
    img: 'https://upload.wikimedia.org/wikipedia/commons/thumb/c/c9/Bugatti_Veyron_16.4_%E2%80%93_Frontansicht_%281%29%2C_5._April_2012%2C_D%C3%BCsseldorf.jpg/520px-Bugatti_Veyron_16.4_%E2%80%93_Frontansicht_%281%29%2C_5._April_2012%2C_D%C3%BCsseldorf.jpg'
  },
  {
    id: 2,
    type: 'airplane',
    brand: 'Boeing 787 Dreamliner',
    colors: ['red', 'white', 'black', 'green'],
    img: 'https://upload.wikimedia.org/wikipedia/commons/thumb/1/15/All_Nippon_Airways_Boeing_787-8_Dreamliner_JA801A_OKJ_in_flight.jpg/600px-All_Nippon_Airways_Boeing_787-8_Dreamliner_JA801A_OKJ_in_flight.jpg'
  },
  {
    id: 3,
    type: 'train',
    brand: 'USRA 0-6-6',
    colors: ['yellow', 'white', 'black'],
    img: 'https://upload.wikimedia.org/wikipedia/en/thumb/a/a1/UP_4466_Neil916.JPG/600px-UP_4466_Neil916.JPG'
  }
];

class MockApiService {
  public getData = () => {
    return Observable.of(data);
  }
}

// class ApiService {}

describe('AppComponent', () => {

  let component: AppComponent;
  let fixture: ComponentFixture<AppComponent>;

  beforeEach(() => {

    TestBed.configureTestingModule({
      declarations: [AppComponent],
      imports: [
        BrowserModule,
        FormsModule
      ],
      providers: [
        { provide: ApiService, useClass: MockApiService }
      ]
    });

  });

  it(`should have as title 'app'`, async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app.title).toEqual('Trafficmeister');
  }));
  it('should render title in a h1 tag', async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    fixture.detectChanges();
    const compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelector('h1').textContent).toContain('TRAFFICMEISTER');
  }));
  it('should render 3 select tags', async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    fixture.detectChanges();
    const compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelectorAll('select').length).toBe(3);
  }));
});

describe('Operating the selectors', () => {

  let component: AppComponent;
  let fixture: ComponentFixture<AppComponent>;
  let spyOnAdd: jasmine.Spy;

  beforeEach(() => {

    TestBed.configureTestingModule({
      declarations: [AppComponent],
      imports: [
        BrowserModule,
        FormsModule
      ],
      providers: [
        { provide: ApiService, useClass: MockApiService }
      ]
    });

  });

  it('select vehicle type', async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    fixture.detectChanges();
    
    const component = fixture.componentInstance;
    const compiled = fixture.debugElement.nativeElement;
    const typeSlector = compiled.querySelector('.option-type');

    component.modeTypeSelected = 'airplane';
    component.populateForm();

    expect(component.modeBrands.length).toBe(1);
    expect(typeSlector).toBeTruthy();
  }));

  it('select vehicle color', async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    fixture.detectChanges();

    const component = fixture.componentInstance;
    const compiled = fixture.debugElement.nativeElement;
    const colorSlector = compiled.querySelector('.option-color');

    component.modeColorSelected = 'red';
    component.populateForm();

    expect(component.modeBrands.length).toBe(2);
    expect(colorSlector).toBeTruthy();
  }));

  it('select vehicle brand and color', async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    fixture.detectChanges();

    const component = fixture.componentInstance;
    const compiled = fixture.debugElement.nativeElement;
    const colorSlector = compiled.querySelector('.option-color');
    const brandSlector = compiled.querySelector('.option-brand');

    component.modeColorSelected = 'black';
    component.modeBrandSelected = 'Bugatti Veyron';
    component.populateForm();

    expect(component.modeTypes.length).toBe(1);
    expect(colorSlector).toBeTruthy();
    expect(brandSlector).toBeTruthy();
  }));
});
